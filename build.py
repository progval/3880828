#!/usr/bin/env python

from __future__ import with_statement

import pbs
import time
import glob
import requests

ROOT = '/home/compiler/Limnoria'

requests.post('http://localhost:7410/limnoria_debian_builds/',
        data={'status': 'Starting nightly builds'})

def get_source_dir(branch, version):
    return ROOT + '/%s-%s/limnoria/' % (branch, version)
def get_githash():
    return pbs.git('show', '--format=%h').split('\n')[0]

def build_deb(branch, version):
    print 'Building .deb for Limnoria %s (Python %i)' % (branch, version)
    suffix = ('-py3k' if version == 3 else '')
    print '\tPulling changes.'
    pbs.cd(get_source_dir(branch, version))
    pbs.cd('./debian/')
    pbs.git('checkout', '--', '.')
    pbs.git('pull')
    print '\tEditting files.'
    pbs.rm('changelog.new', '-f')
    pbs.rm(ROOT + '/%s-%s/*.deb' % (branch, version), '-f')

    githash = get_githash()

    with open('changelog.new', 'a') as fd:
        fd.write('limnoria (0.83.4.1+limnoria+%s~git%s) unstable; urgency=low'
                % (branch, githash))
        fd.write('\n\n  * Nightly build\n\n')
        fd.write(time.strftime(' -- Valentin Lorentz <progval@progval.net>  %a, %d %b %Y %H:%m:%S %z'))
        fd.write('\n\n')
        fd.write(open('changelog', 'r').read())
    pbs.mv('changelog', 'changelog.old')
    pbs.mv('changelog.new', 'changelog')

    if version == 3:
        with open('control.new', 'a') as fd:
            text = open('control', 'r').read() \
                    .replace('python (>= 2.6)', 'python3 (>= 3.2)') \
                    .replace('python (>=2.6)', 'python3 (>=3.2)') \
                    .replace('python (>=2.6)', 'python3 (>=3.2)') \
                    .replace('python (>=2.6.6-3~)', 'python3 (>=3.2)') \
                    .replace('python-', 'python3-') \
                    .replace('XS-Python-Version: >=2.6', 'XS-Python-Version: >=3.2') \
                    .replace('python3-support (>= 0.6), ', '') \
                    .replace('python3-support (>= 0.90.0), ', '')
            fd.write(text)
        pbs.mv('control', 'control.old')
        pbs.mv('control.new', 'control')
        """
        for filename in glob.glob('../scripts/supybot*'):
            text = ''.join(open(filename, 'r').readlines()[1:])
            pbs.rm(filename)
            with open(filename, 'a') as fd:
                fd.write('#!/usr/bin/env python3\n' + text)"""

    print '\tRunning debuild.'
    try:
        pbs.debuild('-i', '-us', '-uc', '-b')
    finally:
        print '\tRestoring files.'
        if version == 3:
            pbs.cd('../')
            pbs.cd('debian')
        pbs.mv('changelog.old', 'changelog')
        if version == 3:
            pbs.mv('control.old', 'control')

    print '\tMoving .deb.'
    pbs.cd(ROOT + '/%s-%s' % (branch, version))
    deb_name = 'limnoria_0.83.4.1+limnoria+%s~git%s_all.deb' % (branch, githash)
    deb_name2 = 'limnoria%s_0.83.4.1+limnoria+%s~git%s_all.deb' % (suffix, branch, githash)
    head_deb_path = '/home/compiler/public/limnoria/debian/python%d/limnoria%s-%s-HEAD.deb' % (version, suffix, branch)
    pbs.rm(head_deb_path, '-f')
    pbs.ln('-s', deb_name2, head_deb_path)
    pbs.mv(deb_name, '/home/compiler/public/limnoria/debian/python%d/%s' % (version, deb_name2))
    print '\tDone.'

FEDORA_TARGET = '/home/compiler/public/limnoria/fedora/python%d/%s'
def build_rpm(branch, version):
    print 'Building .rpm for Limnoria %s (Python %i)' % (branch, version)
    suffix = ('-py3k' if version == 3 else '')
    print '\tPulling changes.'
    pbs.cd(get_source_dir(branch, version))
    print '\tBuilding.'
    getattr(pbs, 'python%d' % version)('setup.py', 'bdist_rpm')
    pbs.cd('dist')
    files = glob.glob('limnoria-*.rpm')
    print('\tMoving .rpm')
    for file_ in files:
        pbs.mv(file_, FEDORA_TARGET % (version, file_))
        head = 'limnoria-%s-HEAD%s' % (branch, file_.rsplit('-1', 1)[1])
        head = FEDORA_TARGET % (version, head)
        pbs.rm(head, '-f')
        pbs.ln('-s', FEDORA_TARGET % (version, file_), head)
    print('\tDone.')

failures = []
for (branch, version) in (('testing', 3),
                          ('master', 3),
                          ('testing', 2),
                          ('master', 2)):
    try:
        build_rpm(branch, version)
    except Exception as e:
        failures.append(('rpm', branch, version, e))
    try:
        build_deb(branch, version)
    except Exception as e:
        failures.append(('deb', branch, version, e))

if failures:
    status = 'Failures: %s' % ', '.join(['%s: %s-%i (%s)' % x for x in failures])
else:
    status = 'All builds passing'
requests.post('http://localhost:7410/limnoria_debian_builds/',
        data={'status': status})
